# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Installation and Boot Manager

Topic 101: System Architecture

101.1 Design hard disk layout

Topic 102: Linux Installation and Package Management

102.2 Install a boot manager

 * [Installation_cloud_images_urls.pdf](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_2_Installation_and_boot_manager/Installation_cloud_images_urls.pdf)
 * [Installation_cloud_images.pdf](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_2_Installation_and_boot_manager/Installation_cloud_images.pdf)
 * [HowTo-ASIX_Arrancada_grub](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_2_Installation_and_boot_manager/HowTo-ASIX-Arrencada-Grub.pdf)
 * [LPI-102.2-Install_a_boot_manager.pdf](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_2_Installation_and_boot_manager/LPI-102.2-Install_a_boot_manager.pdf)
 * [24-Apunts_M05UF3-NF4A05-15_maquines_virtuals.pdf](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF2_2_Installation_and_boot_manager/24-Apunts_M05UF3-NF4A05-15_maquines_virtuals.pdf)
 
