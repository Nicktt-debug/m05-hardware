# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Virtualization

Topic 102: Linux Installation and Package Management

102.6 Linux as a virtualization guest

330.1 Virtualization conceps teory

330.3 KVM

330.4 Other virtualization solutions

330.5 Libvirt & Related tools

 * Imatges del cloud 
 * Sistemes Live prefabricats
 * Virt-Manager / KVM / libvirt
 * VirtualBox

 
