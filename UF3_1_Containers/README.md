# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Containers

Topic 102: Linux Installation and Package Management

  102.6 Linux as a virtualization guest


Topic 352: Container Virtualization

  352.1 Container Virtualization Concepts

  352.3 Docker

  352.4 Container Orchestration Platforms


#### Continguts

 * Docker
 * Docker-compose
 * Swarm

 
