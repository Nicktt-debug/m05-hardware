# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


###  Arquitectura i dispositius

Topic 101: System Architecture

101.1 Determine and configure hardware settings

 * [LPI 101.1 Determine and configure hardware settings](https://learning.lpi.org/en/learning-materials/101-500/101/101.1/)

 * [IBM Learn Linux, 101: Configure hardware settings](https://developer.ibm.com/tutorials/l-lpic1-101-1/)

 * [Apunts-Exercicis LPI-101.1 Determine and configure hardware settings](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_1_Arquitectura_i_dispositius/LPI-101.1%20Determine%20and%20configure%20hardware%20settings.pdf)

