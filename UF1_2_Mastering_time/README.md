# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


### Mastering Time

Topic 108: Essential System Services

108.1 Maintain system time

 * [LPI 108.1 Maintain system time](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1-2_Mastering_time/LPI-108.1-Mantain_system_time.pdf)
 
 * [Apunts professor Mastering time](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1-2_Mastering_time/05-Apunts_M05UF1-NF1A01-04_masteringTime.pdf)
 
 * [Apunts Toolbox Mastering time](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1-2_Mastering_time/06-Apunts_M05UF1-NF1A01-04_ToolBox_masteringTime.pdf)

 * [Apunts Archlinux: System time](https://wiki.archlinux.org/title/System_time)
 
 * [Apunts Fedora Project: Configuring the date and time](https://docs.fedoraproject.org/en-US/fedora/f27/system-administrators-guide/basic-system-configuration/Configuring_the_Date_and_Time/)
 
 * [Apunts IBM Developworks: Mantain system time](https://developer.ibm.com/tutorials/l-lpic1-108-1/)

 * [Apunts LPI: 108.1 Maintain system time](https://learning.lpi.org/en/learning-materials/102-500/108/108.1/)
 
 * [New - Exercicis Mastering time](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1-2_Mastering_time/exercicis_mastering_time.md)
 
 * [Old - Exercicis Mastering time](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1-2_Mastering_time/07-Exercicis_M05UF1-NF1A01-04_masteringTime.txt)
