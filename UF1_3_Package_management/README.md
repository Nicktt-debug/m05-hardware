# M05-hardware
## @edt ASIX M05-Hardware Curs 2021-2022

Podeu trobar la documentació del mòdul a [ASIX-M05](https://sites.google.com/site/asixm05edt/)

ASIX M05-Hardware Escola del treball de barcelona


### Package Management

102-Linux_installation_and_package_management

  102.4-Use_debian_package_management

  102.5-Use_RPM_and_YUM_package_management
  
  102.3-Manage_shared_libraries


 
 * [102.4-Use_debian_package_management](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/LPI-102.4-Package_deb_apt.pdf)
 
 * [102.5-Use_RPM_and_YUM_package_management](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/102.5-Use_RPM_and_YUM_package_management.pdf)
 
 * [Gestió de software](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/Gesti%C3%B3%20del%20software.pdf)
 
 * [102.3-Manage_shared_libraries](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/102.3-Manage_shared_libraries.pdf)
 
 * [shared_libraries.md](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/shared_libraries.md)
 
 * [exercicis_deb.md](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/exercicis_deb.md)
 
 * [exercicis_rpm.md](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/exercicis_rpm.md)
 
 * [exercicis_dnf.md](https://gitlab.com/edtasixm01/m05-hardware/-/blob/main/UF1_3_Package_management/exercicis_dnf.md)
 
 
